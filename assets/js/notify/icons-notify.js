$(function(){
    $(".btn").on("click",function(){
        $.notify({
            icon: 'glyphicon glyphicon-star',
            message: "Copied HTML"
        },{
            type: 'primary',
            animate: {
                enter: 'animated fadeInUp',
                exit: 'animated fadeOutRight'
            },
            placement: {
                from: "bottom",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
        });
    });
});
